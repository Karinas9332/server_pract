/* 
    Необходимо найти FREE REST API в просторах интернета. 

    Пускай эта FREE REST API сможет дать вам полный CRUD (Create, Read, Update, Delete)

    При проверке домашнего задания наставник должен иметь возможность увидеть список
    полученных данных с сервера на html странице.
    
    Так же с нее он должен иметь возможность удалить запись, создать новую,
    или же обновить запись.
*/
const requestUrl = 'https://gorest.co.in/public-api/users'
const postWrapper = document.getElementById('posts-wrapper');
let posts = [];
let btnDelete;
const btnCreate = document.getElementById('btn-create');
const btnUpdate = document.getElementById('btn-update');

const createTemplate = data => {
    return template = `
    <div class="wrapper-post" data-id="${data.id}">
    <div class="id"> Index:${data.id}</div>
    <div class="name"> Name:${data.name}</div>
    <div class="email"> Email:${data.email}</div>
    <button class="btn-delete">Delete Post</button>
    </div>
    `
}

const getPosts = url => {
    fetch(url)
        .then(response => response.json())
        .then(json => {
            posts = json.data;
            posts.forEach(post => {
                postWrapper.innerHTML += createTemplate(post)
            })
            btnDelete = document.querySelectorAll('.btn-delete');
        })
        .then(() => {
            for (let elem of btnDelete) {
                elem.addEventListener('click', e => {
                    let idElem = e.target.parentNode.dataset.id;
                    deletePost(requestUrl, idElem);
                })
            }
        })
}

getPosts(requestUrl);

const deletePost = (url, id) => {
    fetch(url + '/' + id, {
        method: 'DELETE',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'Authorization': 'b11c0089c7ca1d4471f959a55c3e89876a1df6ec7053dc05a33a6908e71377d6',
            'Accept': 'application/json',
        }
    })
}

const createPost = url => {
    inputName = document.getElementById('name').value
    inputGender = document.getElementById('gender').value;
    inputEmail = document.getElementById('email').value;
    inputStatus = document.getElementById('status').value;
    const createObj = {
        name: inputName,
        gender: inputGender,
        email: inputEmail,
        status: inputStatus,
    };
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(createObj),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer b11c0089c7ca1d4471f959a55c3e89876a1df6ec7053dc05a33a6908e71377d6',
            'Accept': 'application/json',
        }
    })
        .then((response) => response.json())
        .then((json) => console.log(json.data))
}
btnCreate.addEventListener('click', e => {
    e.preventDefault();
    createPost(requestUrl);
})

const updatePost = url => {
    inputNameUpdate = document.getElementById('nameUpdate').value
    inputEmailUpdate = document.getElementById('emailUpdate').value
    inputIdPostUpdate = document.getElementById('idUpdate').value;
    inputStatusUpdate = document.getElementById('statusUpdate').value;
    const createObj = {
        id: inputIdPostUpdate,
        name: inputNameUpdate,
        email: inputEmailUpdate,
        status: inputStatusUpdate,
    };
    fetch(url + '/' + inputIdPostUpdate, {
        method: 'PUT',
        body: JSON.stringify(createObj),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer b11c0089c7ca1d4471f959a55c3e89876a1df6ec7053dc05a33a6908e71377d6',
            'Accept': 'application/json',
        }
    })
        .then((response) => response.json())
        .then((json) => console.log(json.data))
}
btnUpdate.addEventListener('click', e => {
    e.preventDefault();
    updatePost(requestUrl)
})

