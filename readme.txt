
/* 
    Необходимо найти FREE REST API в просторах интернета. 

    Пускай эта FREE REST API сможет дать вам полный CRUD (Create, Read, Update, Delete)

    При проверке домашнего задания наставник должен иметь возможность увидеть список
    полученных данных с сервера на html странице.
    
    Так же с нее он должен иметь возможность удалить запись, создать новую,
    или же обновить запись.
*/
